<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('users')->insert([
            'name' => 'James',
            'email' => 'uwajacques@gmail.com' ,
            'password' => bcrypt('Password123'),
        ]);
        
        DB::table('users')->insert([
            'name' => 'Shesh Tech',
            'email' => 'admin@sheshtech.com' ,
            'password' => bcrypt('P@$$w0rd321'),
        ]);
    }
}
