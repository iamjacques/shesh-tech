<?php

namespace App\Http\Controllers;

use PDF;
use Storage;
use Auth;
use Session;
use Redirect;
use Carbon\Carbon;
use Response;
use \App\Report;
use Illuminate\Http\Request;


class GeneratorCtrl extends Controller
{

    public function __construct(){
        return $this->middleware('auth');
    }
    
    
    public function create(Request $request){
        $date = Carbon::now()->format('d/m/Y'); 
        return view('pdf.create', compact(['date']));
    }

    public function gen(Request $request){
        return $request->html;
    }

    public function generate(Request $request){

        $title = $request->title;
        $description = $request->description;
        $inputs = collect($request->all());

        $inputs->forget('_token');
        $inputs->forget('title');
        $inputs->forget('description');

        $formatted = [];
        foreach($inputs as $input){
           $data = [
               'heading' => $input['heading'],
           ];

           $i = 0;
           foreach($input['keys'] as $key){
                $field = [
                    $key => $input['values'][$i]
                ];
                $data['fields'][] = $field;
                $i++;
           }
           array_push($formatted, $data); 
        }

        $data = [
            'title' => $title,
            'description' => $description,
            'data' => $formatted
        ];

        $pdf = PDF::loadView('pdf.document', $data, [
            'author'           => 'Shesh Tech',
	        'subject'          => $title,
        ]);

        $file_name = str_slug( $title.' '. time()).'.pdf';
        $path = 'docs/'. $file_name;
        Report::create(['name'=> $data['title'], 'description'=> $data['description'], 'fields'=> serialize($formatted),  'generated_by'=> Auth::user()->name,'location_path' =>  $path  ]);

        $pdf->save($path);
        Session::flash('download',  $path);
        
        return redirect('/home');
    }

    public function edit($id){
        $report = Report::find($id);
        $fields = unserialize($report->fields);
        $date = Carbon::now()->format('d/m/Y'); 
        return view('pdf.edit', compact(['date', 'report', 'fields']));
    }

    public function postEdit(Request $request){

        $report = Report::find($request->_id);
        $title = $request->title;
        $description = $request->description;
        $inputs = collect($request->all());

        $inputs->forget('_id');
        $inputs->forget('_token');
        $inputs->forget('title');
        $inputs->forget('description');

        //return $inputs;
        $formatted = [];
        foreach($inputs as $input){
           $data = [
               'heading' => $input['heading'],
           ];

           $i = 0;
           foreach($input['keys'] as $key){
                $field = [
                    $key => $input['values'][$i]
                ];
                $data['fields'][] = $field;
                $i++;
           }
           array_push($formatted, $data); 
        }   

        $data = [
            'title' => $title,
            'description' => $description,
            'data' => $formatted
        ];

        $pdf = PDF::loadView('pdf.document', $data, [
            'author'           => 'Shesh Tech',
	        'subject'          => $title,
        ]);
        
        if(file_exists(public_path($report->location_path))){
            unlink(public_path($report->location_path));
        }


        $file_name = str_slug( $title.' '. time()).'.pdf';
        $path = 'docs/'. $file_name;

        $report->fill([
            'name'=> $data['title'], 
            'description'=> $data['description'], 
            'fields'=> serialize($formatted),  
            'generated_by'=> Auth::user()->name,
            'location_path' =>  $path 
        ]);

        $report->save();
        //Report::update(['name'=> $data['title'], 'description'=> $data['description'], 'fields'=> serialize($formatted),  'generated_by'=> Auth::user()->name,'location_path' =>  $path  ]);

        $pdf->save($path);
        Session::flash('download',  $path);
        
        return redirect('/home');

        return $request->all();
    }

    public function delete($id){
        $report = Report::find($id);
        if(file_exists(public_path($report->location_path))){
            unlink(public_path($report->location_path));
        }
        
        $report->delete();
        return redirect('/home');

    }
}
