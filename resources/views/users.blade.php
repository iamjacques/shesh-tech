@extends('layouts.app')

@section('content')
<div class="container">
    <a href="/new/user" class="btn btn-primary btn-sm  new-report"> 
        <i class="ion-ios-plus-outline"></i> New User 
    </a>

    <table class="table">
        <thead>
            <tr>
            <th scope="col"> Name</th>
            <th scope="col">Email</th>
            <th scope="col">User Type</th>
            <th scope="col edit"> Edit </th>
            </tr>
        </thead>
        <tbody>
            @foreach($users as $user)
            <tr>
                <th scope="row"> {{$user->name}} </th>
                <td>{{$user->email}}</td>
                <td>{{$user->userType}}</td>
                <td> 
                    <a href="/user/{{$user->id}}" class="btn btn-primary btn-sm "> 
                        Edit 
                    </a>
                </td>
            </tr>
            @endforeach 
        </tbody>
    </table>
</div>
@endsection
