<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Shesh Tech - PDF Generator </title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel='shortcut icon' type='image/x-icon' href='/fav.ico' />

    <link rel="stylesheet" href="/css/app.css">

    <link href="/css/ionicons.min.css" rel="stylesheet" >
    <style>
    .flex-text-wrap {
    position: relative;

    *zoom: 1;
}

textarea,
.flex-text-wrap {
    outline: 0;
    margin: 0;
    border: none;
    padding: 0;

    *padding-bottom: 0 !important;
}

.flex-text-wrap textarea,
.flex-text-wrap pre {
    white-space: pre-wrap;
    width: 100%;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;

    *white-space: pre;
    *word-wrap: break-word;
}

.flex-text-wrap textarea {
    overflow: hidden;
    position: absolute;
    top: 0;
    left: 0;
    height: 100%;
    width: 100%;
    resize: none;

    /* IE7 box-sizing fudge factor */
    *height: 94%;
    *width: 94%;
}

.flex-text-wrap pre {
    display: block;
    visibility: hidden;
}

textarea,
.flex-text-wrap pre {
    /*
     * Add custom styling here
     * Ensure that typography, padding, border-width (and optionally min-height) are identical across textarea & pre
     */
}

        .page{
            width: 210mm;
            min-height: 297mm;
            padding: 2cm;
            margin: 1cm auto;
            border: 1px #D3D3D3 solid;
            border-radius: 5px;
            background: white;
            box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
            display: -ms-flexbox; /* IE10 */
            display: flex;
            flex-direction: column;
            justify-content: space-between;
        }

        .header{
            display: -ms-flexbox; /* IE10 */
            display: flex;
            flex-direction: row;
            align-items: center;
            justify-content: space-between;
            flex: 0;
        }

        .body{
            flex: 1;
            padding-top: 30px;
            margin-bottom: 40px;
        }

        .description{
            height: auto;
        }

        .description textarea{
            width: 100%;
            resize: none;

    overflow: hidden;
    min-height: 50px;
    max-height: 1000px;
        }
        .addSection{
            display: flex;
        }
        .addSection .fa-plus-circle{
            font-size: 15px;
            cursor: pointer;
            transition: .5s;
        }

        .addSection .fa-plus-circle:hover{
            /* font-size: 25px; */
            color: #0ca9fab5;
        }

        

        form .heading{
            width: 100%;
            margin: 20px 0px 10px 0px;
            border: none;
            border-bottom:none;
            line-height: 30px;
            font-size: 25px;
        }


        .title, .description textarea{
            border: none;
        }

   

        form .heading:focus, form .fields:focus, .description textarea:focus, .title:focus{
            outline: 0px;
        }

        form .heading::placeholder {
            font-size: 25px;
            line-height: 30px;
        }

       form .fields{
            width: 99%;
            border: none;
            width: 99%;
            border: none;
            padding: 3px;
        }


        table {
            border-collapse: collapse;
            width: 100%;
            border: 1px solid black;
            margin-bottom: 5px;
        }


        table, td, th {
                border: 1px solid black;
        }
        
        .show{
            display: block;
        }
        .hide{
            display: none;
        }
        .addSection p{
            margin: 0 10px 5px 10px;
            cursor: pointer;
            color: #a4a4a4b5;
        }
        .delete{
            cursor: pointer; 
            font-size: 20px;
            transition: .5s;
        }
        .delete:hover{
            font-size: 24px;
        }

        .addBtns{
            cursor: pointer;
            display: flex;
        }

        .btnList{
            padding-left: 15px;
        }

        .addSection p:hover{
            color: #0ca9fab5;
        }
        .addField{
            float: right;
        }

        .footer{
            flex: 0;
        }

        .report-name, .company-into {
            display: -ms-flexbox; /* IE10 */
            display: flex;
            flex-direction: row;
            align-items: center;
            justify-content: space-between;
        }

        .generate{
            position: fixed;
            font-size: 40px;
            top: 40px;
            right: 20px;
            height: 60px;
            width: 60px;
            border-radius: 50%;
            display: flex;
        align-items: center;
        justify-content: center;
     padding: 0;

        }
        .generate:focus{
           outline:none;
        }
    </style>

    </section>
</div></head>
    <body>
    
    <form method="POST" autocomplete="off" >
    {{ csrf_field() }}
  

        <div class="page">
        <button class="generate btn btn-primary" > <i class="ion-ios-download-outline"> </i> </button>

            <section class="header"> 
                <h1 class="item"> <input type="text" autofocus placeholder="Form Title" class="title"  name="title"> </h1>
                <img class="item" height="100" src="https://sheshtech.com/wp-content/uploads/2017/10/Shesh-Tech-Positive.png">
            </section>

            <section class="body"> 
                <div class="description"> 
                    <textarea  name="description" class="description" value="description"  placeholder="Form description..." >Form description. This description could be a few lines long and will be standard text across all the report of this type.</textarea>
                </div>
                <div class="addFields"> 
                    <div class="dynamicField">  </div>

                    <div class="addSection">
                        <button type="button" class="btn btn-primary btn-sm addHeading "> 
                            <i class="ion-ios-plus-outline"></i> Heading 
                        </button>
                    </div>
                </div>
            </section>
            
            <section class="footer">
            <div class="report-name">
                <p> Report Name | Page [#] </p>
                <p> {{$date}} </p>
            </div>

            <div class="company-into">
                <p> +44 (0) 121 573 0081 </p>
                <p> info@sheshtech.com </p>
                <p> www.sheshtech.com </p>
            </div>

            <div class="company-disclemer">
                <p> Shesh Tech is a trading name of Clements Innovations Ltd. Registered in England and Wales. Company No. 10837349 </p>
            </div>
             
            </section>
        </div>
        </form>
    </body>

    <script
  src="https://code.jquery.com/jquery-3.3.1.min.js"
  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
  crossorigin="anonymous"></script>
        
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

    <script src="/js/textarea.js" ></script>
   
    <script>
        $(document).ready(function(){

            $(window).keydown(function(event){
                if(event.keyCode == 13) {
                    event.preventDefault();
                    return false;
                }
            });
          
            $('textarea').flexText();

            $('.generate').click(function(e){
                if($('.title').val().length === 0 ){
                    e.preventDefault();
                }
            })
            
            let headingIndex = 1;
    
            $('.addHeading').click(function(e){
                e.preventDefault();
                $( `<input type="text" placeholder="Heading" class="heading"  name="form${headingIndex}[heading]">`).appendTo( ".dynamicField" )

                $('.dynamicField').append(`
                <div class="table-container" style="margin-bottom: 40px">
                    <table id="table" data-index="${headingIndex}" >
                        <tbody class="body">
                            <tr>
                                <td>
                                    <input type="text" placeholder="Form Field" class="fields"  name="form${headingIndex}[keys][]">
                                </td>
                                <td style="position: relative; ">
                                    <input type="text" placeholder="Input" class="fields"  name="form${headingIndex}[values][]">
                                    <a class="delete" style=" position: absolute; top: 4px; right: -25px;"> <i class="ion-ios-trash-outline" aria-hidden="true"></i> </a>
                                </td>
                            </tr>
                        </tbody>               
                    </table>
                    <button  class="addField btn btn-outline-secondary btn-sm " >Add Field</button>
                </div>
                `)
                headingIndex += 1;
            }) 

            $(document).on('click',  '.delete', function(e){
                e.preventDefault();
                $(this).closest('tr').remove();
            });

            $(document).on('click',  '.addField', function(e){
                let table = $(this).closest('.table-container').children('table');
                let index = table.data('index')
                let field = `
                <tr>
                <td><input type="text" placeholder="Form Field" class="fields"  name="form${index}[keys][]"></td>
                <td style="position: relative; ">
                    <input type="text" placeholder="Input" class="fields"  name="form${index}[values][]">
                    <a class="delete"  style="position: absolute; top: 4px; right: -25px;"> <i class="ion-ios-trash-outline" aria-hidden="true"></i> </a>
                </td> 
                </tr>`;

                $(field).appendTo(table);
                e.preventDefault();
            }); 
        })
    </script>
</html>