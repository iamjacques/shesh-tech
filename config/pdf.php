<?php

return [
	'mode'                  => 'utf-8',
	'format'                => 'A4',
	'author'                => 'Shesh Tech',
	'subject'               => 'Report',
	'keywords'              => 'Report',
	'creator'               => 'Shesh Tech',
	'display_mode'          => 'fullpage'
];
